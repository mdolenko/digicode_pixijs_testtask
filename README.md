# PIXI SHAPES - Test Task #

**Version 1.0.0**

Fall down shapes from top to bottom.
 
Shape types:
3 sides, 4 sides, 5 sides, 6 sides, circle, ellipse, random. 
Functions: -/+ increase or decrease the number of shapes generated per second and -/+ increase or decrease the gravity value and display.

Displays summary area of all shapes and counter of shapes. 

## Installation 

1. Download from repository https://mdolenko@bitbucket.org/mdolenko/digicode_pixijs_testtask.git or Unzip archive file from email;
2. Run index.html file;

## Usage

* In the top left you have two HTML text fields, one showing the number of shapes being displayed in the
  rectangle. The other text field shows the surface area (in px^2) occupied by the shapes;
* Buttons in right side -/+ increase or decrease the number of shapes generated per second;
* Buttons in left side -/+ increase or decrease the gravity value;
* Click on the shape, removes her;
* Click on the scene creates new shape;

## Repository

* git clone https://mdolenko@bitbucket.org/mdolenko/digicode_pixijs_testtask.git

## Screenshots

* Screenshot №1 ![image](https://bitbucket.org/mdolenko/digicode_pixijs_testtask/raw/5cd8b060728516effd5129cb985d8de9decd5d7f/Screenshots/digicode_testtask_screen_1.jpg)
* Screenshot №2 ![image](https://bitbucket.org/mdolenko/digicode_pixijs_testtask/raw/0ee0010bfd29729c9319df99ca9671d458910364/Screenshots/digicode_testtask_screen_2.jpg)
* Screenshot №3 ![image](https://bitbucket.org/mdolenko/digicode_pixijs_testtask/raw/0ee0010bfd29729c9319df99ca9671d458910364/Screenshots/digicode_testtask_screen_3.jpg)
* Screenshot №4 ![image](https://bitbucket.org/mdolenko/digicode_pixijs_testtask/raw/0ee0010bfd29729c9319df99ca9671d458910364/Screenshots/digicode_testtask_screen_4.jpg)