'use strict';

(function() {
    var app = {
        // Method for connection all needed methods.
        init: function() {
            this.main();
            this.event();

            model.loadScene(); // Loading main scene.
        },

        // Method for writen unbound with this conception code.
        main: function() { 
            
        },

        // Method for registration event handlers. Registration events in browser.
        event: function() {
            var btnNumberOfShapesPerSecIncrease = document.getElementById("btnNumberOfShapesPerSecIncrease");
            btnNumberOfShapesPerSecIncrease.onclick = controller.numberOfShapesPerSec.increase;

            var btnNumberOfShapesPerSecDecrease = document.getElementById("btnNumberOfShapesPerSecDecrease");
            btnNumberOfShapesPerSecDecrease.onclick = controller.numberOfShapesPerSec.decrease;

            var btnGravityIncrease = document.getElementById("btnGravityIncrease");
            btnGravityIncrease.onclick = controller.gravity.increase;

            var btnGravityDecrease = document.getElementById("btnGravityDecrease");
            btnGravityDecrease.onclick = controller.gravity.decrease;
        },
    };

    app.init();
}());

