function showNotification(closeOnClick, displayCloseButton, positionClass, duration, theme, title, message) {
    window.createNotification({
        closeOnClick: closeOnClick,
        displayCloseButton: displayCloseButton,
        positionClass: positionClass,
        showDuration: duration,
        theme: theme
    })({
        title: title,
        message: message
    });
}
