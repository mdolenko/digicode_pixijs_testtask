'use strict';

var variables = { // Global.
    app: new PIXI.Application(800, 600), // Creating global variable for current scene.
    numberOfShapesPerSecondsMiliseconds: 1000, // Default number of shapes per seconds.
    numberOfShapesPerSecondsCount: 1,
    previousDateTime: Date.now(),
    gravity: 1, // Default gravitation.
    convasWidth: 800, // window.innerWidth; // Screen width.
    convasHeight: 600, //window.innerHeight; // Screen height.
}

var arrays = {
    shapes: []
}

class Coordinates {
    constructor(x, y) {
        this.x = x,
        this.y = y
    }
}