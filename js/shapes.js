// 'use strict';
//import {ColorReplaceFilter} from '@pixi/filter-color-replace';
// import {Container} from 'pixi.js';

class Shape {
    //#region Constructor
    constructor(type, color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY) {
        this.inAreaX = 800 - 100; // Possible coordinate X, which can take the shape.
        this.shapeX = Math.floor(Math.random() * this.inAreaX); // Random coordinate X for shape.
        this.shapeY = -70; // Start coordinate Y for shape.
        this.currentArea = 0; // Area for shape.
        this.color = color; // Shape color.
        this.borderColor = borderColor;
        this.app = app; // Global variable for current scene.
        this.newGuid = utils.ids.newGUID(); // A unique identificator for the shape.
        this.graphics = new PIXI.Graphics(); // Object for draw primitive shape. 
        this.coordinates = []; // Array coordinates for shape.
        this.type = type;
        this.centerShapeY = centerShapeY;

        // Check of coordinates arguments is exists.
        if(centerShapeX !== undefined && centerShapeY !== undefined) { 
            this.coordinates.push(centerShapeX); // Add X.
            this.coordinates.push(centerShapeY); // Add Y.
        }

        this.graphics.beginFill(this.color, 1); // Pour shape color.

        // FUNCTION
        this.firstCoordinates = function() {
            let firstXYOjbect = {
                x: 0,
                y: 0,
                isUserClickOnScene: false
            };

            // If set coordinates after user click on the scene.
            if (this.coordinates.length) { 
                firstXYOjbect.isUserClickOnScene = true;
                firstXYOjbect.x = this.coordinates[0];
                firstXYOjbect.y = this.coordinates[1];
            } else {
                firstXYOjbect.x = this.shapeX;
                firstXYOjbect.y = this.shapeY;
            }

            return firstXYOjbect;
        } // kEnd firstCoordinates method.

        this.configurateShape = function() {
            this.graphics.endFill(); // End drawing.
            this.graphics.interactive = true;
            this.graphics.buttonMode = true; // Change the cursor when hovering.
            this.graphics.live = true;
    
            // Event Handler MOUSEOVER
            this.graphics.mouseover = function(e){
                //this.alpha = .6;
                this.fill.color = "15777370"
                this.updateLineStyle(5, 0xffffff, 1);
            }
    
            // Event Handler MOUSEOUT
            this.graphics.mouseout = function(e){
                this.alpha = 1;
                this.updateLineStyle(0, 0xffffff, 0);
            }
    
            this.app.stage.addChild(this.graphics); // Is displayed on the canvas.
    
            PIXI.Graphics.prototype.updateLineStyle = function(lineWidth, color, alpha){
                let len = this.graphicsData.length;
                for (var i = 0; i < len; i++) {
                    var data = this.graphicsData[i];
                    // data.lineWidth = lineWidth;
                    data.lineColor = color;
                    data.alpha = alpha;
                    this.dirty++;
                    this.clearDirty++;
                }
            }
        } // End DrawShape method.

        var currentGuid; 
        currentGuid = this.guid;

        // EVENT HANDLER
        this.graphics.on('pointerdown', function(e) { // Processing mouse click.
            funcDeleteShape(currentGuid);
        });
    }
    //#endregion Constructor.

    //#region Methods
    area() {
        return this.currentArea;
    }

    changeColor(color) {
      //const color = new PIXI.filters.ColorMatrixFilter();
        // color.desaturate();
        // graphic.filters = [color];
        // let colorReplaceFilter = new ColorReplaceFilter(this.color, color, 1);
        // this.graphics.filters = [colorReplaceFilter];
    }

    get shapeRadius() {
        return this.radius;
    }
    //#endregion Methods

    //#region Properties
    get guid() {
        return this.newGuid;
    }

    set positionX(x) {
        this.graphics.position.x += x;
    }

    set positionY(y) {
        this.graphics.position.y += y;
    }

    get positionY() {
        var positionY = this.graphics.position.y;
            if (this.centerShapeY != undefined) {
              positionY = positionY + this.centerShapeY;
            }

        return positionY;
    }

    get graphicElement() {
        return this.graphics;
    }

    get shapeType() {
        return this.type;
    }

    get centerY() {
        return this.centerShapeY;
    }
    //#endregion Properties
}

// CIRCLE
class Circle extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY, radius) {
        super("circle", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.radius = radius;
    }

    draw() {
        let firstXYObject = this.firstCoordinates();
        this.graphics.drawCircle(firstXYObject.x, firstXYObject.y, this.radius); // Draw.
        this.configurateShape();
    }

    area() {
        return (this.radius * this.radius) * Math.PI;
    }
}

// ELIPSE
class Elipse extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY, horizontal, vertical) {
        super("elipse", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    draw() {
        let firstXYObject = this.firstCoordinates();
        this.graphics.drawEllipse(firstXYObject.x, firstXYObject.y, this.horizontal, this.vertical); // Draw.
        this.configurateShape();
    }

    area() {
        let elipseWidth = this.horizontal;
        let elipseHeight = this.vertical;
        return elipseWidth * elipseHeight * Math.PI;
    }
}

// TRIANGLE
class Triangle extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY) {
        super("triangle", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
    }
    
    draw() {
        var firstXYObject = this.firstCoordinates();

        if(firstXYObject.isUserClickOnScene == true) {
            firstXYObject.y = firstXYObject.y - 30;
        }

        this.graphics.moveTo(firstXYObject.x, firstXYObject.y);
        this.coordinates.push(firstXYObject.x, firstXYObject.y);

        for (var i = 0; i < 2; ++i) {
            var offsetRandomX =  utils.random.range(30, 80);
            var offsetRandomY =  utils.random.range(30, 70);
            if (i == 1) {
                this.graphics.lineTo(firstXYObject.x - offsetRandomX, firstXYObject.y + offsetRandomY);
                this.coordinates.push(firstXYObject.x + offsetRandomX, firstXYObject.y - offsetRandomY);
            } else {
                this.graphics.lineTo(firstXYObject.x + 20, firstXYObject.y + offsetRandomY);
                this.coordinates.push(firstXYObject.x + 20, firstXYObject.y + offsetRandomY);
            }
        }
        this.graphics.lineTo(firstXYObject.x, firstXYObject.y);
        this.configurateShape();
    } // End draw.

    area() {
        const x1 = this.coordinates[0];
        const y1 = this.coordinates[1];
        const x2 = this.coordinates[2];
        const y2 = this.coordinates[3];
        const x3 = this.coordinates[4];
        const y3 = this.coordinates[5];

        let side1 = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        let side2 = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
        let side3 = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));

        let p = (side1 + side2 + side3) / 2;
        let area = Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
        return area;
    }
}

// RECTANGLE
class Rectangle extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY, width, height) {
        super("rectangle", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.width = width;
        this.height = height;
    }
    
    draw() {
        let firstXYObject = this.firstCoordinates();

        if(firstXYObject.isUserClickOnScene == true) {
            firstXYObject.y = firstXYObject.y - 30;
            firstXYObject.x = firstXYObject.x - 40;
        }

        this.graphics.drawRect(firstXYObject.x, firstXYObject.y, this.width, this.height);
        this.configurateShape();
    } // End draw.

    area() { 
        return this.width * this.height;
    }
}

// PENTAGONE (5 points)
class Pentagon extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY, radius) {
        super("pentagon", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.radius = radius;
    }

    draw() {
        let firstXYObject = this.firstCoordinates();

        if (firstXYObject.isUserClickOnScene == true) {
            firstXYObject.y = firstXYObject.y - 30;
        }

        this.graphics.moveTo(firstXYObject.x, firstXYObject.y);

        for(let i = 1; i <= 5; ++i){
            let th = i * 2 * Math.PI / 5;
            let x = firstXYObject.x + this.radius * Math.sin(th);
            let y = firstXYObject.y + this.radius - this.radius * Math.cos(th);
            this.coordinates.push(x, y);
            this.graphics.lineTo(x, y);
        }

        this.configurateShape();
    } // End draw.

    area() { //
        const x1 = this.coordinates[0];
        const y1 = this.coordinates[1];
        const x2 = this.coordinates[2];
        const y2 = this.coordinates[3];

        let sideLength = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        let area = (Math.sqrt(5 * (5 + 2 * (Math.sqrt(5)))) * sideLength * sideLength) / 4;
        return area;
    }
}

// HEXAGON (6 points)
class Hexagon extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY, radius) {
        super("hexagon", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.radius = radius;
    }

    draw(raius) {
        let firstXYObject = this.firstCoordinates();

        if(firstXYObject.isUserClickOnScene == true) {
            firstXYObject.y = firstXYObject.y - 30;
        }

        this.graphics.moveTo(firstXYObject.x, firstXYObject.y);

        for(let i = 1; i <= 6; ++i){
            let th = i * 2 * Math.PI / 6;
            let x = firstXYObject.x + this.radius * Math.sin(th);
            let y = firstXYObject.y + this.radius - this.radius * Math.cos(th);
            this.coordinates.push(x, y);
            this.graphics.lineTo(x, y);
        }

        this.configurateShape();
    } // ENd draw.

    area() {
        let x1 = this.coordinates[0];
        let y1 = this.coordinates[1];
        let x2 = this.coordinates[2];
        let y2 = this.coordinates[3];

        let sideLength = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        return ((3 * Math.sqrt(3) * (sideLength * sideLength)) / 2);
    }
}

// POLIGON
class Poligon extends Shape {
    constructor(color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY) {
        super("poligon", color, borderColor, app, funcDeleteShape, centerShapeX, centerShapeY);
        this.vertixCountFactor = 0.6;
        this.#resetPathData();
    }

    draw() {
        let firstXYObject = this.firstCoordinates();

        this.#resetPathData();
        this.coordinates = this.#generateCoords(firstXYObject.x, firstXYObject.y);

        this.graphics.drawPolygon(this.coordinates);
        this.configurateShape();
    } // End draw.

    area() { // Poligone area. // Not finished.
        let area = 0;
        var points =  utils.random.range(3, 12);
        let Ys = [];
        let Xs = [];

        for (let i = 0; i < this.coordinates.length; i++) {
            Xs.push(this.coordinates[i]);
            i++;
        }

        for (let i = 0; i < this.coordinates.length; i++) {
            i++
            Ys.push(this.coordinates[i]);
        }

        let arrX = Ys;
        let arrY = Xs;
        // Calculate value of formula.
        let j = points - 1;
        for (let i = 0; i < points; i++) {
            area += (arrX[j] + arrX[i]) * (arrY[j] - arrY[i]);
            j = i;  // j is previous vertex to i
        }
        return Math.abs(area / 2.0); // Return absolute value
    }

    //#region Draw poligone.
    #generateCoords = function(centerX, centerY) {
        const radius = utils.random.range(30, 45);

        for (let i = 0; i < 2*Math.PI; i+= this.vertixCountFactor) {

            let x = (radius * Math.cos(i) + centerX) + this.#getRandomRadiusModifier();
            let y = (radius * Math.sin(i) + centerY) + this.#getRandomRadiusModifier();

            this.pathDArray.push({x,y});
            if (i + this.vertixCountFactor >= 2 * Math.PI) {
                this.pathDArray.push(this.pathDArray[0])
            };
        };

        var coordinatesArrey = [];

        this.pathDArray.forEach(coord => {
            coordinatesArrey.push(coord.x);
            coordinatesArrey.push(coord.y);
        });

        return coordinatesArrey;
    }

    #getRandomRadiusModifier = function() {
        let num = Math.floor(Math.random() * 7) + 1;
        num *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
        return num;
    }

    #drawLinearShape = function() {
        this.pathD = "M";
        this.pathDArray.forEach(coord => {
            this.pathD += `${coord.x},${coord.y} `;
        })
    }

    #resetPathData = function() {
        this.pathD = "";
        this.pathDArray = [];
        this.pathDString = "";
    };
    //#endregion Draw poligone.
}