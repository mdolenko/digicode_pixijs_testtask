'use strict';

var utils = {
    // RANDOM
    random: {
        count : function(count) {
            return Math.floor(Math.random() * count);
        },
        range: function(from, to) {
            return Math.floor(Math.random() * (to - from) + from);
        },
        color: function() {
            const letters = '0123456789ABCDEF';
            let color = '0x';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
    }, // END radom

    // TIMERS
    timers: {
        sleep: function(milliseconds) {
            const date = Date.now();
            let currentDate = null;
            do {
                currentDate = Date.now();
            } while (currentDate - date < milliseconds);
        }
    }, // END timers

    // IDS
    ids: {
        newGUID: function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    }, // END ids

    // ELEMENTS
    elements: {
        offset: function(element) {
            let rect = element.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
        }
    }, // END elements

    // NOTIFICATIONS
    notifications: {
        notification: function(closeOnClick, displayCloseButton, positionClass, duration, theme, title, message) {
            showNotification(closeOnClick, displayCloseButton, positionClass, duration, theme, title, message);
        }
    }
}


