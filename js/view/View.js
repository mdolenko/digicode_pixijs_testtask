'use strict';

// VIEW (what the user sees)
var view = {
    // OUTPUT
    output: {
        shapes: {
            count: function(countOfShapes) {
                document.getElementById("input_shapes_count").value = countOfShapes;
            },
            area: function(area) {
                document.getElementById("input_shapes_area").value = area;
            },
        },
        gravity: function(gravity) {
            document.getElementById("input_gravity").value = gravity;
        },
        numberOfShapesPerSec: function(numberOfShapesPerSecondsCount) {
            document.getElementById("input_shapes_per_seconds").value = numberOfShapesPerSecondsCount;
        },
    } // End output.
}