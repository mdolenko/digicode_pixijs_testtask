'use strict';

// CONTROLLER (user events)
var controller = {
    // SHAPE
    shape: {
        // When user click on the shape.
        clear: () => model.clearShape(this),
        add: () => { // When user click on the scene.
            const x = event.clientX; // Get the horizontal coordinate.
            const y = event.clientY; // Get the vertical coordinate.
            model.shape.add(x, y);
        },
    },

    // GRAVITYf
    gravity: {
        increase: () => model.gravity.increase(),
        decrease: () => model.gravity.decrease(),
    },
    
    // NUMBER OF SHAPSE PER SEC
    numberOfShapesPerSec: {
        increase: () => model.numberOfShapesPerSec.increase(),
        decrease: () => model.numberOfShapesPerSec.decrease(),
    },
} // END Controller