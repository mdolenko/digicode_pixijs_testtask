'use strict';
//import "./node_dodules/dist/filter-color-replace.js";
//const { filterColorREplace } = require('./node_dodules/dist/filter-color-replace.js')
//import { ColorReplaceFilter } from '././node_dodules/dist/filter-color-replace.js';
//var filter = require('@pixi/filter-color-replace');

// MODEL (business logic)
var model = {
    // LOAD SCENE
    loadScene: function() {
      model.createCanvas(); // Create convas.

      // TICKER
      variables.app.ticker.add(function() {
          model.shape.generates();
      });
    },

    // SCENE CONSTRUCTION
    createCanvas: function() { // Convas.
        // APPLICATION
        const scene = document.getElementById("scene");
        scene.appendChild(variables.app.view); // Outputs it to the body of the page.

        // GRAPHICS
        let graphics = new PIXI.Graphics();
        graphics.hitArea = new PIXI.Rectangle(0, 0, variables.convasWidth, variables.convasHeight);
        graphics.interactive = true;
        variables.app.stage.addChild(graphics);
        variables.app.renderer.backgroundColor = 0xfcfcfc;

        // EVENTS 
        graphics.on("pointerdown", function(e) { // Clicking on the convas.
            controller.shape.add();
        });
    },
    
    // SHAPE
    shape: {
      // ADD SHAPE
      add: function(firstX, firstY) {
        const color = utils.random.color(); // Take a random color.
        const borderColor = utils.random.color(); // Take a random border color.

        //#region Shape center offset.
        // CENTER COORDINATES OFFSET
        if(firstX !== undefined && firstY !== undefined){ // Check of coordinates arguments is exists.
            // Get position of scene for coordinates correction when user click on the scene.
            const canvas = document.querySelector('canvas');
            const canvasOffset = utils.elements.offset(canvas);
            firstX = firstX - canvasOffset.left;
            firstY = firstY - canvasOffset.top;
        }
        //#endregion Shape center offset.

          var shape = null;
          const radius = utils.random.range(20, 45);

          function drawCircle() {
              let circle = new Circle(color, borderColor, variables.app, deleteShape, firstX, firstY, radius);
              circle.draw();
              shape = circle;
          }

          switch(utils.random.range(0, 7)) { // From 0 to 7.
          //switch(6) { // For testing.
              case 0: // CIRCLE
                  drawCircle();
              break;
  
              case 1: // ELIPSE
                  const horizontal = utils.random.range(20, 50);
                  const vertical = utils.random.range(15, 20);
                  let elipse = new Elipse(color, borderColor, variables.app, deleteShape, firstX, firstY, horizontal, vertical);
                  elipse.draw();
                  shape = elipse;
              break;
                  
              case 2: // TRIANGLE
                  let triangle = new Triangle(color, borderColor, variables.app, deleteShape, firstX, firstY);
                  triangle.draw();
                  shape = triangle;
              break;
  
              case 3: // RECTANGLE
                  const width = Math.floor((Math.random() * 60) + 50);
                  const height = Math.floor((Math.random() * 30) + 50);
                  let rectangle = new Rectangle(color, borderColor, variables.app, deleteShape, firstX, firstY, width, height);
                  rectangle.draw();
                  shape = rectangle;
              break;
  
              case 4: // PENTAGONE +
                  let pentagon = new Pentagon(color, borderColor, variables.app, deleteShape, firstX, firstY, radius);
                  pentagon.draw();
                  shape = pentagon;
              break;
  
              case 5: // HEXAGON +
                  let hexagon = new Hexagon(color, borderColor, variables.app, deleteShape, firstX, firstY, radius);
                  hexagon.draw();
                  shape = hexagon;
              break;
  
              case 6: // POLIGONE +
                  let poligon = new Poligon(color, borderColor, variables.app, deleteShape, firstX, firstY);
                  poligon.draw();
                  shape = poligon;
              break;
  
              default:
                  drawCircle();
              break;
          }

        arrays.shapes.push(shape); // Add shape to array of all shapes.

        // VIEW
        view.output.shapes.count(model.shapes.count()); // Display of number of shapes.
        view.output.shapes.area(model.shapes.area.summ(arrays.shapes)); // Displays the amount of area.

        // Delete shape.
        function deleteShape(guid) {
          
          var currentShape = arrays.shapes.find(function(element) {
            return element.guid == guid;
          });

          if (currentShape == null) {
            throw new UserException("Shape not found in array of shapes.");
          }

          var newColor = utils.random.color();
          arrays.shapes.forEach(element => { 
            if (element.shapeType == currentShape.shapeType) {
              element.graphicElement.tint = newColor;
            }
          });

          // Delete shepe after click on the shape.
          model.shape.delete(currentShape);
        }
      },
      // DELETE SHAPE
      delete: function(shape) { 
        shape.graphicElement.clear(); // Erase shape from convas.
        shape.graphicElement.removeChild(shape); // Erase shape from convas.
        
        arrays.shapes.splice(arrays.shapes.findIndex(v => v.guid === shape.guid), 1);

        view.output.shapes.count(model.shapes.count());
        view.output.shapes.area(model.shapes.area.summ(arrays.shapes));
      },
      // GENERATE SHAPE
      generates: function(){
        const offset = 70;

        // DROW SHAPE AFTER TIME INTERVAL
        function drowShapeByMiliseconds() { // Drow shape after expiration interval time.
          var milliseconds = variables.numberOfShapesPerSecondsMiliseconds;
          let currentDate = null;
          currentDate = Date.now();
          
          if(currentDate - variables.previousDateTime > variables.numberOfShapesPerSecondsMiliseconds) {
              variables.previousDateTime = Date.now();
              model.shape.add();
          }
        }
        
        // Delete shape after fall.
        function clearShapeAfterFall(shape) {
            if (shape.positionY > variables.convasHeight + offset) {
                model.shape.delete(shape);
            }
        }

        // WORK OF GRAVITY
        for (var i = 0; i < arrays.shapes.length; i++) { // Count of created shapes.
            if (arrays.shapes[i] != undefined) {
                arrays.shapes[i].positionY = variables.gravity; // Work of gravity.
                clearShapeAfterFall(arrays.shapes[i]);
            }
        }

        drowShapeByMiliseconds();
      }
    },

    // GRAVITY
    gravity: {
      increase: function() {
        if (variables.gravity == 20) {
          utils.notifications.notification(true, false, "nfc-top-right", 3000, "info", "Notification", "This is max value.");
          return;
        }

        variables.gravity++;
        view.output.gravity(variables.gravity);
      },
      decrease: function() {
        if (variables.gravity == 1) { 
          utils.notifications.notification(true, false, "nfc-top-right", 3000, "info", "Notification", "This is min value.");
          return;
        }

        variables.gravity--;
        view.output.gravity(variables.gravity);
      },
      work: function() {
        for (var i = 0; i < arrays.shapes.length; i++) { // Count of created shapes.
          if (arrays.shapes[i] != undefined) {
              arrays.shapes[i].positionY = gravity; // Сompel gravity work.
              clearShapeAfterFall(arrays.shapes[i], i);
          }
      }

      drowShapeByMiliseconds();
      }
    },

    // NUMBER OF SHAPES PER SEC
    numberOfShapesPerSec: {
      increase: function() {
          if (variables.numberOfShapesPerSecondsMiliseconds == 50) {
            utils.notifications.notification(true, false, "nfc-top-right", 3000, "info", "Notification", "This is max value.");
            return;
          }

          if (variables.numberOfShapesPerSecondsMiliseconds == 500) {
              variables.numberOfShapesPerSecondsMiliseconds = variables.numberOfShapesPerSecondsMiliseconds - 50;
          }

          variables.numberOfShapesPerSecondsMiliseconds = variables.numberOfShapesPerSecondsMiliseconds - 100;
          let count = ++variables.numberOfShapesPerSecondsCount;
          view.output.numberOfShapesPerSec(count);
      },  
      decrease: function() {
          if (variables.numberOfShapesPerSecondsCount == 1) {
            utils.notifications.notification(true, false, "nfc-top-right", 3000, "info", "Notification", "This is min value.");
            return;
          }

          variables.numberOfShapesPerSecondsMiliseconds = variables.numberOfShapesPerSecondsMiliseconds + 100;

          let count = --variables.numberOfShapesPerSecondsCount;
          view.output.numberOfShapesPerSec(count);
      },
    },

    // AREA
    shapes: {
      area: { 
        summ: function(shapes) {
            let area = 0;
            for (let index = 0; index < shapes.length; index++) {
                area = area + shapes[index].area();
            }
            return Math.round((area) * 100) / 100;
        }
      },
  
      // COUNT
      count: function() {
        let count = arrays.shapes.length; // Count of shapes.
        return count;
      }
    },
}